<?php

// conf site
$site_url = 'http://cslrp.ddns.net:8000';
$site_tagline = 'CSL-RP';
$site_title = 'Comunidade de Software Livre de Ribeirão Preto';
$site_theme = 'default';

// conf navbar
$navbar = [
    'home' => 'Inicio',
    'contato' => 'Contato',
    'sobre-nos' => 'Sobre nós',
];

// conf posts
$posts_per_page = 6;
$total_posts = count(file('data/post.data'));
$last_page = ceil($total_posts/$posts_per_page);
