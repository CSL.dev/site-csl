<?php

/* Função: get_content
 * Uso: get_content()
 * Retorna: conteúdo da pagina atual ('home', etc..)
 * Descrição: Captura o valor do conteudo
 */
function get_content(){
    return (isset($_GET['cont'])) ? $_GET['cont'] : 'home';
}

/* Função: get_page
 * Uso: get_page()
 * Retorna: o slug da pagina atual ('home', etc..)
 * Descrição: Captura o valor do conteudo
 */
function get_page(){
    return (isset($_GET['page'])) ? $_GET['page'] : 'home';
}

/* Função: get_post_content
 * Uso: get_post_content()
 * Retorna: nada
 * Descrição: Mostra o conteúdo principal de uma pagina
 */
function show_main_content($content_type){
    $pd = new Parsedown();

    if($content_type == 'posts'){
        $content = get_content();
        echo $pd->text(file_get_contents($content_type.'/'.$content.'/'.$content.'.md'));
    }
    else{
        $content = get_page();
        echo $pd->text(file_get_contents($content_type.'/'.$content.'.md'));
    }
}

/* Função: show_content
 * Uso: show_content()
 * Retorna: <nada>
 * Descrição: Incluir o template correspondente ao conteudo da página
 */
function show_page(){
    $page = get_page();
    if($page == 'home'){
        include 'inc/page-type/posts.php';
    }
    else{
        include 'inc/page-type/'.$page.'.php';
    }
}

function navbar_gen(){
    global $navbar, $site_url;
	$item_template = '<li><a %shref="%s">%s</a></li>'.PHP_EOL;

    $current_page = get_page();
    $navbar_generated = '';

    foreach($navbar as $slug => $label){
        $url = ($slug == 'home') ? $site_url : $site_url.'/?cont=page&amp;page='.$slug;
        $class = ($slug == $current_page) ? 'class="current-page" ' : '';
        $navbar_generated .= sprintf($item_template, $class, $url, $label);
    }
                  
    echo $navbar_generated;
}

function print_single_post_preview($post_line){
    if($post_line != ''){
        global $site_url;
        echo '<li class="post-preview">';

        $post_content = explode('|', $post_line);


        echo '<h3 class="post_title"><a href="'.$site_url.'/?cont='.$post_content[0].'&amp;page=post">'.$post_content[3].'</a></h3>';
        echo '<p class="post_summary">'.$post_content[7].'</p>';    
        echo '<hr>';

        $categories = explode(',', $post_content[5]);
        $spanned_categories = '';
        foreach ($categories as $c)
            $spanned_categories .= '<span class="post_categorie">'.$c.'</span>';

        echo '<p>'.$spanned_categories.'<span class="post_date">'.$post_content[1].'</span></p>';
                             
        echo '</li>';
    }
}

function print_posts_preview($database, $post_page){
    global $posts_per_page;
    //navega até a página de desejo
    for($p = 1; $p<$post_page; $p++){
        for($i = 1; $i<=$posts_per_page; $i++){
            fgets($database);
        }
    }
    for($i = 0; $i<$posts_per_page; $i++){
        print_single_post_preview(fgets($database));
    }
}

function get_post_page_num(){
    $content = get_content();
    return ($content == 'home') ? 1 : str_split($content,4)[1];
}

function ls_posts(){
    global $site_url, $posts_per_page;

    $page_num = get_post_page_num();

    $database = fopen('data/post.data', 'r');
    
    print_posts_preview($database, $page_num);
    fclose($database);
}

// pager

function previous_page($page_atual){
    $page_final = ($page_atual == 1) ? $page_atual : ($page_atual - 1);
    echo '?cont=page'.$page_final.'&amp;page=posts';
}

function next_page($page_atual){
    global $last_page;
    $page_final = ($page_atual == $last_page) ? $page_atual : ($page_atual + 1);
    echo '?cont=page'.$page_final.'&amp;page=posts';
}

function goto_page($page_final){
    echo '?cont=page'.$page_final.'&amp;page=posts';
}
