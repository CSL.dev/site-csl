<?php
include 'settings.php';
include 'lib/parsedown.php';
include 'cms/functions.php';
include 'inc/header.php';
?>

<div id="content-wrapper">

  <div id="content">
    
    <div id="main">

      <?php 
        show_page();
      ?>
       
    </div><!--main-->

    <?php include 'inc/sidebar.php';?>

    <div class="cleaner"></div>

  </div><!--contents-->

</div><!--contents-wrapper-->

<?php include 'inc/footer.php';?>
