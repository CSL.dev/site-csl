<!DOCTYPE html>
<!--
  ____ ____  _
 / ___/ ___|| |
| |   \___ \| |
| |___ ___) | |___
 \____|____/|_____|
-->

<html lang="pt-BR">  

  <head>
    <meta charset="UTF-8" />
    <title><?=$site_tagline?></title>
    <link rel="icon" href="favicon.png"/>
    <link rel="stylesheet" type="text/css" href="themes/<?=$site_theme?>/styles.css"/>
  </head>
  
  <body>
    <div id="all">

      <div id="header-wrapper">
	<div id="header">
     <a href="<?=$site_url?>"><div id="site-title-img"></div></a>
	</div><!--header-->
      </div><!--header-wrapper-->
      
      <?php include 'inc/navbar.php';?>
