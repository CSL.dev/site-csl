<div id="sidebar">
    <div class="widget">
       <h3 class="widget-title"><a href="https://codeberg.org/CSL.dev">CSL.dev</a></h3>
       <p>A CSL.dev é uma ala da CSL voltada ao desenvolvimento de projetos
	 de software livre. Acesse nossa <a href="https://codeberg.org/CSL.dev">
	 organização no Codeberg</a> e venha conhecer e colaborar com nossos projetos!
       </p>
    </div>

    <div class="widget">
       <h3 class="widget-title"><a href="https://odysee.com/@CSL-RP:6">Acompanhe nosso canal no odysee</a></h3>
       <p>O odysee é um <i>frontend</i> para a plataforma LBRY, onde é possível
	 publicar conteúdos (texto, imagens, vídeos e etc) de maneira livre, sem
	 o controle de algorítmos proprietários que tanto restrigem a liberdade
	 de produtores e consumidores de conteúdo, como ocorre no youtube.
	 </p>
    </div>
</div><!-- #sidebar -->
