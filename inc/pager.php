<div class="pager">
    <p>
      <?php $page_atual = get_post_page_num(); ?>
      &laquo; <a href="<?php previous_page($page_atual); ?>">Anterior</a>
      [<?=$page_atual?>]
      <a href="<?php next_page($page_atual); ?>">Próxima</a> &raquo;
    </p>
</div>
