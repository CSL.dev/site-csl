<?php

/*
  Armazenar as seguintes informações sobre os posts em um arquivo:
  -data
  -data de modificação
  -autor
  -categoria
  -tags
  -resumo
  -nome do arquivo
  -caminho do arquvo
  
  arquivo: data/posts.data.php
*/
// spearadores de campos e conteudo
$fs = '|';
$cs = PHP_EOL;
 
// quebra de linhas dupla
$deol = PHP_EOL.PHP_EOL;

// abrindo arquivo de dados para escrita
$h = fopen('../data/post.data', 'w');

foreach(array_reverse(glob('../posts/*/*.md')) as $post_file){
    
    $post_info['date'] = str_split(basename($post_file, '.md'), 10)[0];

    $post_contents = file_get_contents($post_file);
    
    $regex['title'] = '/^# (.*)/m';


    $regex['summary'] = '/# .*?'.$deol.'+(.*?)'.$deol.'+<!--[ ]*more[ ]*?-->/s';

    $header_infos = ['change-date', 'author', 'categories', 'tags'];
    foreach($header_infos as $k){
        $regex[$k] = '/^:'.$k.'=(.*)/m';
    }
    
    foreach($regex as $k => $re){
        preg_match($re, $post_contents, $post_data[$k]);
        $post_info[$k] = trim($post_data[$k][1]);
    }
    $post_info['summary'] = str_replace(["\n", "\r", "|"], [' ', '', '&brvbar;'], $post_info['summary']);
    
    $rec = '';
    $rec .= basename($post_file, '.md').$fs;
    $rec .= $post_info['date'].$fs;
    $rec .= $post_info['change-date'].$fs;
    $rec .= $post_info['title'].$fs;
    $rec .= $post_info['author'].$fs;
    $rec .= $post_info['categories'].$fs;
    $rec .= $post_info['tags'].$fs;
    $rec .= $post_info['summary'].$cs;

    fwrite($h, $rec);

    //print_r($post_info);
}
fclose($h);

