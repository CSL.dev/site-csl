#Quem somos

A CSL foi criada com o propósito de **informar** a comunidade do câmpus
acerca **software livre**, e promover sua adoção em todas as
aplicações onde software proprietário hoje se faz presente, em
particular nas **atividades didáticas** realizadas pelos estudantes
(ensino, realização de experimentos, elaboração de relatórios, etc).

Consideramos o uso de soluções proprietárias **incompatível** com os
objetivos declarados de uma universidade pública: de **democratizar**
o acesso ao estudo, de **produzir conhecimento** para o benefício da
**sociedade**, e de **empoderar** seus alunos através do
conhecimento. Software proprietário, todavia, priva o usuário do
**direito de estudar e compreender os programas que executa**, e
**restringe** sua liberdade de **melhorá-lo** e **compartilhá-lo**

Portanto, o uso de software proprietário pela universidade não só
constitui na **perda de oportunidades de aprendizado**, como acarreta
em uma relação de **poder** do proprietário do software em relação ao
usuário. Este poder é frequentemente **abusado**, e a única maneira de
evitar estes abusos é através das **liberdades** e **transparência**
que o **software livre** proporciona.

CSL busca combater este problema através de uma abordagem **de alunos
para alunos**; Somos uma iniciativa **criada por alunos** interessados
na causa, e nosso principal público alvo é **o corpo estudantil** (não
se limitando a ele, entretanto).
